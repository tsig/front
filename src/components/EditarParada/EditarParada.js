import React from "react";
import ReactDOM from "react-dom";
import "ol/ol.css";
import Map from "ol/Map";
import View from "ol/View";
import * as olProj from "ol/proj";
import { Tile as TileLayer, Vector as VectorLayer } from "ol/layer";
import { Circle as CircleStyle, Fill, Stroke, Style } from "ol/style";
import { OSM, Vector as VectorSource, Source } from "ol/source";
import Point from "ol/geom/Point";
import Feature from "ol/Feature";
import { Draw, Modify, Snap } from "ol/interaction";
import { Toast, Modal } from "bootstrap";
import { Link } from "react-router-dom";

class EditarParada extends React.Component {
  constructor(props) {
    super(props);
    this.ubicacionParada = "";
  }
  componentDidMount() {
    var toast = new Toast(document.getElementById("toastMessage"), {});
    toast.show();
    //obtenermos el id desde la url
    function extractUrlValue(key, url) {
      if (typeof url === "undefined") url = window.location.href;
      var match = url.match("[?&]" + key + "=([^&]+)");
      return match ? match[1] : null;
    }

    const idParada = extractUrlValue("id").replace("parada.", "");

    const posParada = [
      parseFloat(extractUrlValue("x")),
      parseFloat(extractUrlValue("y")),
    ];

    this.ubicacionParada = posParada;

    var iconFeature = new Feature({
      geometry: new Point(posParada),
    });
    var source = new VectorSource({
      features: [iconFeature],
    });
    var posicionParada = new VectorLayer({
      source: source,
      style: new Style({
        fill: new Fill({
          color: "rgba(255, 255, 255, 0.2)",
        }),
        stroke: new Stroke({
          color: "#ffcc33",
          width: 2,
        }),
        image: new CircleStyle({
          radius: 7,
          fill: new Fill({
            color: "#ffcc33",
          }),
        }),
      }),
    });

    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);

    var mapa = new TileLayer({
      source: new OSM(),
    });
    // agregar la parada con el punto que tenemos
    var layers = [mapa, posicionParada];

    var map = new Map({
      layers: layers,
      target: "map",
      view: new View({
        center: pos,
        zoom: 16,
      }),
    });

    var modify = new Modify({ source: source });
    map.addInteraction(modify);

    var draw, snap; // global so we can remove them later
    var typeSelect = "Point";

    function addInteractions() {
      draw = new Draw({
        source: source,
        type: typeSelect,
      });
      map.addInteraction(draw);
      snap = new Snap({ source: source });
      map.addInteraction(snap);

      draw.on("drawend", function (e2) {
        var feature = e2.feature;
        var geometry = feature.getGeometry();

        this.ubicacionParada = geometry.getCoordinates();

        // modifico la parada en la base
        var ubicacionParada = {
          type: "point",
          coordinates: this.ubicacionParada,
        };
        const body = {
          punto: ubicacionParada,
        };

        const settings = {
          method: "PUT",
          headers: {
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json",
          },
          cache: "no-cache",
          body: JSON.stringify(body),
        };

        console.log(body);
        const url = "http://localhost:4567/paradas/editParada/" + idParada;
        console.log(settings);
        fetch(url, settings)
          .then((res) => {
            console.log("Res url: " + url);
            console.log(res);
          })
          .then((data) => {
            console.log(data);
          });

        var modal = new Modal(document.getElementById("successModal"), {});

        modal.show();
      });
    }

    addInteractions();
  }

  render() {
    return (
      <div>
        <div id="map" className="map"></div>
        <div className="position-fixed bottom-0 end-0 p-3">
          <div
            id="toastMessage"
            className="toast"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
          >
            <div className="toast-body">
              Mueva la parada a la nueva ubicaci&oacute;n y para confirmar haga
              clic nuevamente sobre la parada modificada.
              <div className="mt-2 pt-2 border-top d-grid gap-2">
                <Link to={"/home"} className="link btn btn-danger btn-sm">
                  Salir
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="successModal"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content text-center">
              <div class="modal-header">
                <h5 class="modal-title ">Parada Modificada</h5>
              </div>
              <div className="modal-body d-grid gap-2 text-center">
                <p>Se modific&oacute; la parada correctamente</p>
                <a
                  onClick={() => (window.location.href = "/home")}
                  className="link btn btn-success"
                >
                  Volver al inicio
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditarParada;
