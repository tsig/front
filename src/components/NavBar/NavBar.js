import logo from "../../favicon.ico";
import "./NavBar.css";
import { Link } from "react-router-dom";

function NavBar(props) {

  const LogOut = () => {
    localStorage["isLogged"] = "false";
    localStorage["username"] = "none";
    window.location.replace("/");
  };

  const isLogued = () => {
    if (localStorage["isLogged"] == "true") {
      return (
        <div className="dropdown dropstart">
          <button
            className="btn btn-danger dropdown-toggle"
            type="button"
            id="dropdownMenuButton1"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            {localStorage["username"]}
          </button>
          <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
            <li>
              <Link to="/RegistrarLinea" className="nav-link link">
                Registrar una Linea
              </Link>
            </li>
            <li>
              <Link to="/RegistrarParada" className="nav-link link">
                Registrar una Parada
              </Link>
            </li>
            <li>
              <hr class="dropdown-divider"></hr>
            </li>
            <li>
              <a
                type="button"
                className="nav-link active link"
                onClick={LogOut}
              >
                LogOut
              </a>
            </li>
          </ul>
        </div>
      );
    } else {
      return (
        <button
          type="button"
          className=" btn btn-outline-success"
          data-bs-toggle="modal"
          data-bs-target="#login"
        >
          Ingresar
        </button>
      );
    }
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="index.html">
          <img src={logo} alt="" width="60" height="60" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <div className="dropdown navbar-nav me-auto mb-2 mb-lg-0">
            <button
              className="btn btn-success dropdown-toggle"
              type="button"
              id="dropdownMenuButton1"
              data-bs-toggle="dropdown"
              aria-expanded="false"
            >
              Buscar
            </button>
            <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              <li>
                <a onClick={props.toggleShowModalSearchLine} className="nav-link active link">
                  Buscar una línea por código + destino
                </a>
              </li>
              <li>
                <hr class="dropdown-divider"></hr>
              </li>
              <li>
                <Link to="/BuscarCruce" className="nav-link active link">
                  Buscar un cruce
                </Link>
              </li>
              <li>
                <hr class="dropdown-divider"></hr>
              </li>
              <li>
                <a onClick={props.toggleShowModalSearchAddress} className="nav-link active link">
                  Buscar una dirección
                </a>
              </li>
              <li>
                <hr class="dropdown-divider"></hr>
              </li>
              <li>
                <Link to="/BuscarPorEmpresa" className="nav-link active link">
                  Buscar las líneas de una empresa
                </Link>
              </li>
              <li>
                <hr class="dropdown-divider"></hr>
              </li>
              <li>
                <Link to="/BusquedaPorModHora" className="nav-link active link">
                  Buscar lineas y paradas modificadas
                </Link>
              </li>
              <li>
                <hr class="dropdown-divider"></hr>
              </li>
              <li>
                <a onClick={props.toggleDrawLinesPolygon} className="nav-link active link">
                  Buscar las líneas que cortan un polígono
                </a>
              </li>
              <li>
                <hr class="dropdown-divider"></hr>
              </li>
              <li>
                <Link to="/BuscarLineaDestino" className="nav-link active link">
                  Buscar línea que me lleva al destino
                </Link>
              </li>
            </ul>
            <ul class="navbar-nav me-auto ms-3">
              <li class="nav-item">
                <select class="form-select" onChange={(event) => props.toggleFilterParadas(event.target.value)}>
                  <option selected value="true">
                    Paradas Habilitadas
                  </option>
                  <option value="false">
                    Paradas Deshabiltadas
                  </option>
                </select>
              </li>
            </ul>
          </div>
          <form className="d-flex">{isLogued()}</form>
        </div>
      </div>
    </nav>
  );
}

export default NavBar;
