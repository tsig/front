import React, { useState } from "react";

function Login() {
  const [usuario, setUsuario] = useState("");
  const [password, setPassword] = useState("");

  const body = { username: usuario, password: password };

  const login = async () => {
    const settings = {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    };

    const request = await fetch("http://localhost:4567/user/login", settings);
    const response = await request.json();

    console.log(response);
    console.log(response.isLogged);
    if (!response.isLogged) {
      var error = document.getElementById("mensajeError");

      if (error.classList.contains("hide")) {
        error.classList.remove("hide");
        error.classList.add("show");
      }

      console.log(error);
    } else {
      localStorage["isLogged"] = response.isLogged;
      localStorage["username"] = response.username;
      window.location.replace("/");
    }
  };

  return (
    <div
      className="modal fade"
      id="login"
      data-bs-backdrop="static"
      data-bs-keyboard="false"
      tabindex="-1"
      aria-labelledby="staticBackdropLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered">
        <div className="modal-content">
          <div className="modal-header ">
            <div class="form-title text-right">
              <h4>Login</h4>
            </div>
            <button
              type="button"
              className="btn-close"
              data-bs-dismiss="modal"
              aria-label="Close"
            ></button>
          </div>
          <div className="modal-body">
            <form>
              <div className="mb-3">
                <label for="username" className="form-label">
                  User Name
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="username"
                  value={usuario}
                  onChange={(event) => {
                    setUsuario(event.target.value);
                  }}
                ></input>
              </div>
              <div className="mb-3">
                <label for="password" className="form-label">
                  Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  value={password}
                  onChange={(event) => {
                    setPassword(event.target.value);
                  }}
                ></input>
              </div>
              <div
                class="alert alert-danger alert-dismissible fade hide"
                role="alert"
                id="mensajeError"
              >
                <strong>Error:</strong> UserName o Password incorrectos.
              </div>
              <div className="d-grid gap-2">
                <input
                  type="button"
                  className="btn btn-success"
                  onClick={login}
                  value="Ingresar"
                ></input>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
