import React, { useEffect } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import TileLayer from "ol/layer/Tile";
import View from "ol/View";
import * as olProj from "ol/proj";
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import { Vector as VectorLayer } from "ol/layer";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import Snap from "ol/interaction/Snap";
import TileWMS from "ol/source/TileWMS";
import { Modal } from "bootstrap";
import { Link } from "react-router-dom";
import OSM from "ol/source/OSM";
import { map } from "jquery";

class BuscarCruce extends React.Component {
  constructor(props) {
    super(props);
    this.buscarCruce = this.buscarCruce.bind(this);
    this.state = {
      calleuno: "",
      calledos: "",
      mapLayers: [],
      mapaosm: null,
      vez: "",
      map: null
    };
  }
  componentDidMount() {
    // para mostrar el modal apenas carga la pagina
    var modal = new Modal(document.getElementById("buscarCruce"), {});
    modal.show();
    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);
    var source = new VectorSource();


    var map = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        })
      ],
      target: "map",
      view: new View({
        center: pos,
        zoom: 16,
      }),
    });

    this.setState({map: map});
  }

  buscarOtraVez(){
    var modal = new Modal(document.getElementById("buscarCruce"), {});
    modal.show();
  }
  
  buscarCruce = async () =>{
  
    var calle1 = "'" + this.state.calleuno.toUpperCase() + "'";
    var calle2 = "'" + this.state.calledos.toUpperCase() + "'";
    var filter = "nom_calle= " + calle1 + " OR nom_calle=" + calle2;
    console.log(filter);

    const settings = {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      }
    };

    const url = "http://localhost:4567/paradas/getejes/" + this.state.calleuno.toUpperCase() + "/" + this.state.calledos.toUpperCase();

    var response;
    const request = await fetch(url, settings)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if(Object.keys(data).length < 2){
          var modal = new Modal(document.getElementById("errorModal"), {});
          modal.show();
        }else{
          response = data[1].slice(6, -1);
          console.log(response);
          // lo separo en latitude y longitude
          var latlong = response.split(" ");
          const longitude = parseFloat(latlong[0]);
          const latitude = parseFloat(latlong[1]);
    
          console.log("Latitud: ")
          console.log(latitude);
          console.log("Longitud: ")
          console.log(longitude);
    
          var ejes = new TileLayer({
            visible: true,
            source: new TileWMS({
              url: "http://localhost:8080/geoserver/tsig/wms",
              params: {
                FORMAT: "image/png",
                VERSION: "1.1.1",
                tiled: true,
                LAYERS: "tsig:ejes",
                cql_filter: filter ,
              },
            }),
          });
    
          this.state.map.addLayer(ejes);
          this.state.map.setView(new View({
            center: olProj.fromLonLat([longitude, latitude]),
            zoom: 18}));
        }
      });
    
  }

  render(){
    return(
      <div>
        <div id="map" className="map"></div>
        <div
          className="modal fade"
          id="buscarCruce"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Buscar un Cruce</h5>
              </div>
              <div className="modal-body">
                <form>
                  <div className="mb-3">
                    <label for="calleuno" className="form-label">
                      Calle 1
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="calleuno"
                      value={this.state.calleuno}
                      onChange={(event) => {
                        this.setState({ calleuno: event.target.value });
                      }}
                    ></input>
                  </div>
                  <div className="mb-3">
                    <label for="calledos" className="form-label">
                      Calle 2
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="calledos"
                      value={this.state.calledos}
                      onChange={(event) => {
                        this.setState({ calledos: event.target.value });
                      }}
                    ></input>
                  </div>
                  
                  <div className="d-grid gap-2">
                    <button
                      type="button"
                      className="btn btn-success"
                      data-bs-target="#map"
                      onClick={this.buscarCruce}
                      data-bs-dismiss="modal"
                    >
                      Buscar
                    </button>

                    <Link to={"/home"} className="link btn btn-danger btn">
                      Salir
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div> 
        </div>
        <div
          className="modal fade"
          id="errorModal"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Error</h5>
              </div>
              <div className="modal-body d-grid gap-2 text-center">
                <p>No pudimos encontrar un cruce entre las calles solicitadas</p>
                <button
                      type="button"
                      className="btn btn-success"
                      data-bs-target="#map"
                      onClick={this.buscarOtraVez}
                      data-bs-dismiss="modal"
                    >
                      Buscar otra vez
                    </button>
                <Link to="/home" className="link btn btn-danger btn">
                  Cancelar
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BuscarCruce;