import React, { Component } from 'react'
import "ol/ol.css";
import Map from "ol/Map";
import TileLayer from "ol/layer/Tile";
import View from "ol/View";
import * as olProj from 'ol/proj';
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import { Vector as VectorLayer } from "ol/layer";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import Snap from "ol/interaction/Snap";
import TileWMS from "ol/source/TileWMS";
import { Modal } from "bootstrap";
import { Link } from "react-router-dom";
import OSM from "ol/source/OSM";
import { data, map } from "jquery";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import proj4 from 'proj4';
import { buffer } from 'ol/size';

export class BuscarLineaDestino extends Component {
    constructor(props) {
        super(props);
        this.state = {
          seleccion: "",
          calleuno: "",
          calledos: "",
          addressNumber: "",
          addressStreet: "",
          lon: "",
          lat: "",
          buffer: 100,
          response: [],
          responseUsr: [],
          posIni: null,
          pointDir: null,
          pointUsr: null,
        };
        this.buscarLinea = this.buscarLinea.bind(this);
        this.fetchBuscarCruce = this.fetchBuscarCruce.bind(this);
        this.fetchBuscarDireccion = this.fetchBuscarDireccion.bind(this);  
        this.fetchLocalizacionUsr = this.fetchLocalizacionUsr.bind(this);
        this.obtenerLineas = this.obtenerLineas.bind(this);
        this.getCommon = this.getCommon.bind(this);
        this.buscarOtraVez = this.buscarOtraVez.bind(this);
    }
    
    componentDidMount() {
        // para mostrar el modal apenas carga la pagina
        var modal = new Modal(document.getElementById("buscarLineaDestino"), {});
        modal.show();
        // me traigo las coordenadas del localsotrage
        let coordinates = localStorage["coordinates"];
        // lo separo en latitude y longitude
        var latlong = coordinates.split(",");
        const latitude = parseFloat(latlong[0]);
        const longitude = parseFloat(latlong[1]);
        console.log("Latitud Usuario: ")
        console.log(latitude);
        console.log("Longitud Usuario: ")
        console.log(longitude);

        // lo formateo a un formato que acepta openlayers
        var pos = olProj.fromLonLat([longitude, latitude]);     
        
        var map = new Map({
          layers: [
            new TileLayer({
              source: new OSM(),
            })
          ],
          target: "map",
          view: new View({
            center: pos,
            zoom: 16,
          }),
        });
    
        this.setState({ posIni: pos});
        this.setState({ lat: latitude});
        this.setState({ lon: longitude});
        this.setState({map: map});
        this.setState({ seleccion: "Buscar por cruce" });
    }

    buscarOtraVez(){
        var modal = new Modal(document.getElementById("buscarLineaDestino"), {});
        modal.show();
      }

    fetchLocalizacionUsr(latitude, longitude){
        console.log("USUARIO");
        proj4.defs([
            [
                'EPSG:4326',
                '+title=WGS 84 (long/lat) +proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees'
            ],
            [
                'EPSG:32721',
                '+proj=utm +zone=21 +south +datum=WGS84 +units=m +no_defs'
            ]
        ]);
        var pos2 = proj4('EPSG:4326','EPSG:32721',[longitude,latitude]);

        console.log("     Posicion en 32721: " + pos2)

        const body = {
            "type":"Point",
            "coordinates": pos2
        }
        //console.log(body);

        const settings = {
            method: "PUT",
            headers: {
                Accept: "application/json, text/plain, */*",
                "Content-Type": "application/json",
            },
            cache: "no-cache",
            body: JSON.stringify(body),
        };

        const urlBusqueda = "http://localhost:4567/lineas/posUsuario/" + this.state.buffer;
        fetch(urlBusqueda, settings)
            .then((response) => response.json())
            .then((data) => {
                console.log("     Data usr: " + data);
                this.setState({ responseUsr: data });
            });
        
    }

    fetchBuscarCruce(calle1, calle2){
        this.fetchLocalizacionUsr(this.state.lat, this.state.lon);

        console.log("CRUCE");
        const url = "http://localhost:4567/lineas/getejes/" + calle1 + "/" + calle2;
        fetch(url)
        .then((response) => response.json())
        .then((data) => {
            var response = data[1].slice(6, -1);
            console.log(response);
            // lo separo en latitude y longitude
            var latlong = response.split(" ");
            const longitude = parseFloat(latlong[0]);
            const latitude = parseFloat(latlong[1]);
        
            console.log("     Latitud Cruce: " + latitude + "  Longitud Cruce: "  + longitude)

            const body = {
                "type":"Point",
                "coordinates": [longitude, latitude]
             }

            //console.log(body);
            const settings = {
                method: "PUT",
                headers: {
                    Accept: "application/json, text/plain, */*",
                    "Content-Type": "application/json",
                },
                cache: "no-cache",
                body: JSON.stringify(body),
            };

            const urlBusqueda = "http://localhost:4567/lineas/posUsuario/" + this.state.buffer;
            fetch(urlBusqueda, settings)
            .then((response) => response.json())
            .then((data) => {
                console.log("     Data cruce: " + data);
                this.setState({ response: data });
                this.obtenerLineas();
            });
        });
    }

    fetchBuscarDireccion(addrStreet, addrNumber){
        this.fetchLocalizacionUsr(this.state.lat, this.state.lon);
        let url = "https://direcciones.ide.uy/api/v0/geocode/BusquedaDireccion?calle=" + addrStreet + 
                        "%20" + addrNumber + "&departamento=montevideo&localidad=montevideo";

        fetch(url)
        .then((response) => response.json())
        .then((data) => {
            let address = data[0];

            var pos2 = proj4('EPSG:4326','EPSG:32721',[address.puntoX, address.puntoY]);

            console.log("Posicion en 32721: ")
            console.log(pos2);

            const body = {
                "type":"Point",
                "coordinates": pos2
            }

            console.log(body);
            const settings = {
                method: "PUT",
                headers: {
                    Accept: "application/json, text/plain, */*",
                    "Content-Type": "application/json",
                },
                cache: "no-cache",
                body: JSON.stringify(body),
            };

            const urlBusqueda = "http://localhost:4567/lineas/posUsuario/" + this.state.buffer;
            fetch(urlBusqueda, settings)
            .then((response) => response.json())
            .then((data) => {
                console.log("Data: ");
                console.log(data);
                this.setState({ response: data });
                this.obtenerLineas();
            });

        });
    }

    obtenerLineas(){

        if(this.getCommon(Array.from(JSON.parse(JSON.stringify(this.state.responseUsr))), 
        Array.from(JSON.parse(JSON.stringify(this.state.response))))){
            
            var filter =
            "id IN ('" + Array.from(JSON.parse(JSON.stringify(this.state.responseUsr))).join("','") + "') AND id IN ('" + 
            Array.from(JSON.parse(JSON.stringify(this.state.response))).join("','") + "')";
            console.log(filter);

            var linea = new TileLayer({
                visible: true,
                source: new TileWMS({
                    url: "http://localhost:8080/geoserver/tsig/wms",
                    params: {
                    FORMAT: "image/png",
                    VERSION: "1.1.1",
                    tiled: true,
                    LAYERS: "tsig:linea",
                    cql_filter: filter,
                    },
                }),
            });

            this.state.map.addLayer(linea);
            this.state.map.setView(new View({
                    center: this.state.posIni,
                    zoom: 14}));  
        }else if (this.state.buffer <= 1100){
            console.log("no encontró iguales");
            this.setState({ buffer: this.state.buffer+100});
            console.log("Buffer: " + this.state.buffer);
            this.buscarLinea();
        }else{
            this.setState({ buffer: 100});
            var modal = new Modal(document.getElementById("errorModal"), {});
            modal.show();
        }
    

    }

    buscarLinea(){
        console.log("en buscar linea");
        if(this.state.seleccion == "Buscar por cruce"){
            this.fetchBuscarCruce(this.state.calleuno.toUpperCase(), this.state.calledos.toUpperCase());
        }else{
            this.fetchBuscarDireccion(this.state.addressStreet, this.state.addressNumber);
        }      
    }

    getCommon(arr1, arr2) {
        var common = false;  
        console.log(arr1)
        console.log(arr2)                 // Array to contain common elements
        for(var i=0 ; i<arr1.length ; ++i) {
          for(var j=0 ; j<arr2.length ; ++j) {
            if(arr1[i] == arr2[j]) {       // If element is in both the arrays
              common= true;        // Push to common array
            }
          }
        }
        
        return common;                     // Return the common elements
    }

    render() {
        let formCruce, formCalle;
        if(this.state.seleccion == "Buscar por cruce"){
            formCruce = (
                <form id="xCruce">
                    <div className="mb-3">
                        <label for="calleuno" className="form-label">
                        Calle 1
                        </label>
                        <input
                        type="text"
                        className="form-control"
                        id="calleuno"
                        value={this.state.calleuno}
                        onChange={(event) => {
                            this.setState({ calleuno: event.target.value });
                        }}
                        ></input>
                    </div>
                    <div className="mb-3">
                        <label for="calledos" className="form-label">
                        Calle 2
                        </label>
                        <input
                        type="text"
                        className="form-control"
                        id="calledos"
                        value={this.state.calledos}
                        onChange={(event) => {
                            this.setState({ calledos: event.target.value });
                        }}
                        ></input>
                    </div>
                </form>
            );  
            formCalle = ""; 
        }else{
            formCalle = (
                <form id="xCalle">
                    <div className="mb-3">
                    <label htmlFor="" className="form-label">
                        Calle
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="calle"
                        onChange={(event) => {
                            this.setState({ addressStreet: event.target.value });
                        }}
                    />
                    </div>
                    <div className="mb-3">
                    <label htmlFor="numero" className="form-label">
                        Numero
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        id="numero"
                        onChange={(event) => {
                            this.setState({ addressNumber: event.target.value });
                        }}
                    />
                    </div>
                </form>
            );
            formCruce = "";
        }

        return (
            <div>
                <div id="map" className="map"></div>
                <div
                className="modal fade"
                id="buscarLineaDestino"
                data-bs-backdrop="static"
                data-bs-keyboard="false"
                tabindex="-1"
                aria-labelledby="staticBackdropLabel"
                aria-hidden="true"
                >
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Buscar l&iacute;nea que me lleva a Destino</h5>
                    </div>
                    <div className="modal-body">
                        <div className="col-12">
                            <select
                                value={this.state.seleccion}
                                onChange={(event) => {
                                    this.setState({ seleccion: event.target.value });
                                  }}
                                class="form-select"
                                id="lineas_para_agregar"
                            >
                                <option selected>Buscar por cruce</option>
                                <option >Buscar por calle y Nº de puerta</option>
                            </select>
                            <br></br>
                        </div>                       
                        <div>
                          {formCruce}
                        </div>
                        <div>
                          {formCalle}
                        </div> 
                        <div className="d-grid gap-2">
                            <button
                            type="button"
                            className="btn btn-success"
                            data-bs-target="#map"
                            onClick={this.buscarLinea}
                            data-bs-dismiss="modal"
                            >
                            Buscar
                            </button>

                            <Link to={"/home"} className="link btn btn-danger btn">
                            Salir
                            </Link>
                        </div>
                    </div>
                    </div>
                </div> 
                </div>
                <div
                className="modal fade"
                id="errorModal"
                data-bs-backdrop="static"
                data-bs-keyboard="false"
                tabindex="-1"
                aria-labelledby="staticBackdropLabel"
                aria-hidden="true"
                >
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Error</h5>
                    </div>
                    <div className="modal-body d-grid gap-2 text-center">
                        <p>No pudimos encontrar lineas cercanas a su ubicación y destino</p>
                        <button
                            type="button"
                            className="btn btn-success"
                            data-bs-target="#map"
                            onClick={this.buscarOtraVez}
                            data-bs-dismiss="modal"
                            >
                            Buscar otra vez
                            </button>
                        <Link to="/home" className="link btn btn-danger btn">
                        Cancelar
                        </Link>
                    </div>
                    </div>
                </div>
                </div>               
            </div>
        )
    }
}

export default BuscarLineaDestino
