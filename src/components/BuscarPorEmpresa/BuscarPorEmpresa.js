import React, { Component } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import TileLayer from "ol/layer/Tile";
import View from "ol/View";
import * as olProj from "ol/proj";
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import { Vector as VectorLayer } from "ol/layer";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import Snap from "ol/interaction/Snap";
import TileWMS from "ol/source/TileWMS";
import { Modal } from "bootstrap";
import { Link } from "react-router-dom";
import OSM from "ol/source/OSM";
import { map } from "jquery";

export default class BuscarPorEmpresa extends Component {
  constructor(props) {
    super(props);
    this.buscarxEmpresa = this.buscarxEmpresa.bind(this);
    this.state = {
      empresa: "",
      contentLineas: [],
      map: null,
    };
  }
  componentDidMount() {
    // para mostrar el modal apenas carga la pagina
    var modal = new Modal(document.getElementById("buscarxEmpresa"), {});
    modal.show();
    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);
    var source = new VectorSource();

    var map = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      target: "map",
      view: new View({
        center: pos,
        zoom: 16,
      }),
    });

    //this.setState({map: map});
  }

  buscarxEmpresa = async () => {
    const settings = {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    };

    const url = "http://localhost:4567/lineas/xEmpresa/" + this.state.empresa;

    var response;
    const request = await fetch(url, settings)
      .then((res) => res.json())
      .then((data) => {
        response = JSON.stringify(data);
        console.log(data.length);
        if (data.length == 0) {
          console.log(data);
          document.getElementById("lineas-de-empresa").innerHTML =
            "<tr><td colspan='5'><div class='alert alert-danger'>No pudimos encontrar l&iacute;neas de la empresa solicitada.</div></tr></td>";
        } else {
          document.getElementById("lineas-de-empresa").innerHTML = "";
          console.log(data);
          this.setState({ contentLineas: [] });
          data.forEach((element) => {
            var li = element.split(",");
            console.log(li);
            const codigo = li[0];
            const origen = li[1];
            const destino = li[2];
            const empresa = li[3];
            console.log(codigo);
            console.log(origen);
            console.log(destino);
            console.log(empresa);
            let row = (
              <tr>
                <td>{codigo}</td>
                <td>{origen}</td>
                <td>{destino}</td>
              </tr>
            );

            let newlist = this.state.contentLineas;
            newlist.push(row);

            console.log("newlist es");
            console.log(newlist);

            this.setState({ contentLineas: newlist });
          });
        }
      });
  };
  render() {
    let tableLineasDeEmpresa = (
      <table className="table table-light">
        <thead>
          <tr>
            <th scope="col">C&oacute;digo</th>
            <th scope="col">Origen</th>
            <th scope="col">Destino</th>
          </tr>
        </thead>
        <tbody id="lineas-de-empresa">
          {this.state.contentLineas.map((row) => {
            return row;
          })}
        </tbody>
      </table>
    );
    return (
      <div>
        <div id="map" className="map"></div>
        <div
          className="modal fade"
          id="buscarxEmpresa"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Buscar las líneas de una empresa</h5>
              </div>
              <div className="modal-body">
                <form>
                  <div className="row g-3">
                    <div className="mb-3">
                      <label for="empresa" className="form-label">
                        Empresa
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="empresa"
                        value={this.state.empresa}
                        onChange={(event) => {
                          this.setState({ empresa: event.target.value });
                        }}
                      ></input>
                    </div>
                    <div className="col-12 table-responsive">
                      {tableLineasDeEmpresa}
                    </div>
                  </div>

                  <div className="d-grid gap-2">
                    <button
                      type="button"
                      className="btn btn-success"
                      data-bs-target="#map"
                      onClick={this.buscarxEmpresa}
                    >
                      Buscar
                    </button>

                    <Link to={"/home"} className="link btn btn-danger btn">
                      Salir
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
