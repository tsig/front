import React, { useEffect } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import TileLayer from "ol/layer/Tile";
import View from "ol/View";
import * as olProj from "ol/proj";
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import { Vector as VectorLayer } from "ol/layer";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import Snap from "ol/interaction/Snap";
import TileWMS from "ol/source/TileWMS";
import { Modal } from "bootstrap";
import { Link } from "react-router-dom";
import OSM from "ol/source/OSM";
import { map } from "jquery";

class BuscarPorHora extends React.Component {
  constructor(props) {
    super(props);
    this.buscarPorHora = this.buscarPorHora.bind(this);
    this.state = {
      horas: "",
      mapLayers: [],
      map: null,
    };
  }
  componentDidMount() {
    // para mostrar el modal apenas carga la pagina
    var modal = new Modal(document.getElementById("buscarPorHora"), {});
    modal.show();
    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);
    var source = new VectorSource();

    var map = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      target: "map",
      view: new View({
        center: [-6251681.1746235965, -4148062.303116895],
        zoom: 12,
      }),
    });

    this.setState({ map: map });
  }

  buscarPorHora = async () => {
    //console.log(filter);

    const settings = {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
    };

    const url = "http://localhost:4567/lineas/ultimas/" + this.state.horas;

    var response;
    const request = await fetch(url, settings)
      .then((res) => res.json())
      .then((data) => {
        response = JSON.stringify(data);
        console.log(data);
      });
    console.log(JSON.parse(response));

    var filter =
      "codigo IN ('" + Array.from(JSON.parse(response)).join("','") + "')";
    console.log(filter);
    var linea = new TileLayer({
      visible: true,
      source: new TileWMS({
        url: "http://localhost:8080/geoserver/tsig/wms",
        params: {
          FORMAT: "image/png",
          VERSION: "1.1.1",
          tiled: true,
          LAYERS: "tsig:linea",
          cql_filter: filter,
        },
      }),
    });
    console.log(linea);
    this.state.map.addLayer(linea);

    const urlParada =
      "http://localhost:4567/paradas/ultimas/" + this.state.horas;

    var responseParada;
    const requestParada = await fetch(urlParada, settings)
      .then((res) => res.json())
      .then((data) => {
        responseParada = JSON.stringify(data);
        console.log(data);
      });
    console.log(JSON.parse(responseParada));

    var filterParada =
      "id IN ('" + Array.from(JSON.parse(responseParada)).join("','") + "')";

    var parada = new TileLayer({
      visible: true,
      source: new TileWMS({
        url: "http://localhost:8080/geoserver/tsig/wms",
        params: {
          FORMAT: "image/png",
          VERSION: "1.1.1",
          tiled: true,
          LAYERS: "tsig:parada",
          cql_filter: filterParada,
        },
      }),
    });
    console.log(parada);
    this.state.map.addLayer(parada);

    /*var countKey = Object.keys(response).length;
    console.log(countKey);
    if(countKey == 20){
      var modal = new Modal(document.getElementById("errorModal"), {});
      modal.show();
    }else{
      response = response.slice(27, -3);
      console.log(response);
      // lo separo en latitude y longitude
      var latlong = response.split(" ");
      const longitude = parseFloat(latlong[0]);
      const latitude = parseFloat(latlong[1]);

      console.log("Latitud: ")
      console.log(latitude);
      console.log("Longitud: ")
      console.log(longitude);

      var ejes = new TileLayer({
        visible: true,
        source: new TileWMS({
          url: "http://localhost:8080/geoserver/tsig/wms",
          params: {
            FORMAT: "image/png",
            VERSION: "1.1.1",
            tiled: true,
            LAYERS: "tsig:ejes",
            cql_filter: filter ,
          },
        }),
      });

      this.state.map.addLayer(ejes);
      this.state.map.setView(new View({
        center: olProj.fromLonLat([longitude, latitude]),
        zoom: 18}));
    }*/
  };

  render() {
    return (
      <div>
        <div id="map" className="map"></div>
        <div
          className="modal fade"
          id="buscarPorHora"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Buscar Lineas y parada modificadas</h5>
              </div>
              <div className="modal-body">
                <form>
                  <div className="mb-3">
                    <label for="hora" className="form-label">
                      Horas
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="horas"
                      value={this.state.horas}
                      onChange={(event) => {
                        this.setState({ horas: event.target.value });
                      }}
                    ></input>
                  </div>

                  <div className="d-grid gap-2">
                    <button
                      type="button"
                      className="btn btn-success"
                      data-bs-target="#map"
                      onClick={this.buscarPorHora}
                      data-bs-dismiss="modal"
                    >
                      Buscar
                    </button>

                    <Link to={"/home"} className="link btn btn-danger btn">
                      Salir
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default BuscarPorHora;
