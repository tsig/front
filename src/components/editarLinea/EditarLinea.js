import React from "react";
import ReactDOM from "react-dom";
import "ol/ol.css";
import Map from "ol/Map";
import View from "ol/View";
import * as olProj from "ol/proj";
import { Tile as TileLayer, Vector, Vector as VectorLayer } from "ol/layer";
import { Circle as CircleStyle, Fill, Stroke, Style } from "ol/style";
import { OSM, Vector as VectorSource, Source } from "ol/source";
import Point from "ol/geom/Point";
import Feature from "ol/Feature";
import { Draw, Modify, Snap } from "ol/interaction";
import { Toast, Modal, Button } from "bootstrap";
import { Link } from "react-router-dom";
import LineString from "ol/geom/LineString";
import TileWMS from "ol/source/TileWMS";
import { GeoJSON } from "ol/format";
import { bbox } from "ol/loadingstrategy";
import { Select, defaults as defaultInteractions } from "ol/interaction";

class EditarLinea extends React.Component {
  constructor(props) {
    super(props);
    this.recorrido = "";
    this.idLinea = "";
  }

  componentDidMount() {
    var toast = new Toast(document.getElementById("toastMessage"), {});
    toast.show();
    //obtenermos el id desde la url
    function extractUrlValue(key, url) {
      if (typeof url === "undefined") url = window.location.href;
      var match = url.match("[?&]" + key + "=([^&]+)");
      return match ? match[1] : null;
    }

    this.idLinea = extractUrlValue("id").toString().replace("linea.", "");
    localStorage["idLinea"] = "";
    localStorage["idLinea"] = this.idLinea;
    const codigo = extractUrlValue("codigo").toString();
    console.log(codigo);
    let source = new TileWMS({
      url: "http://localhost:8080/geoserver/tsig/wms",
      serverType: "geoserver",
      params: {
        TILED: true,
        LAYERS: "tsig:linea",
        cql_filter: "codigo = " + codigo,
      },
    });
    let linea = new TileLayer({
      visible: true,
      source: source,
    });

    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);
    var mapa = new TileLayer({
      source: new OSM(),
    });
    // agregar la parada con el punto que tenemos

    var vector = new VectorLayer({
      source: new VectorSource({
        url:
          "http://localhost:8080/geoserver/wfs?service=WFS&version=1.1.0&request=GetFeature&typename=tsig:linea&outputFormat=application/json&srsName=urn:x-ogc:def:crs:EPSG:3857&cql_filter=codigo=" +
          codigo,
        format: new GeoJSON(),
        wrapX: false,
      }),
    });

    var select = new Select({
      wrapX: false,
    });

    var modify = new Modify({
      features: select.getFeatures(),
    });

    var layers = [mapa, vector];

    var map = new Map({
      interactions: defaultInteractions().extend([select, modify]),
      layers: layers,
      target: "map",
      view: new View({
        center: [-6251681.1746235965, -4148062.303116895], // centro en montevideo
        zoom: 12,
      }),
    });

    modify.on("modifyend", function (e2) {
      console.log(e2);

      this.recorrido =
        e2.features.array_[0].geometryChangeKey_.target.flatCoordinates;
      console.log(this.recorrido);
      localStorage["recorrido"] = "";
      localStorage["recorrido"] = JSON.stringify(this.recorrido);
      var toast = new Toast(document.getElementById("toastMessage"), {});
      toast.show();
    });
  }

  confirmarCambios() {
    console.log("confirmar");

    // tengo que rearmar el recorrido como array de arrays de puntos.
    var arrayNumber = JSON.parse(localStorage["recorrido"]);

    let parArray = [];
    let noParArray = [];

    arrayNumber.forEach((element, index) => {
      if (index % 2 == 0) parArray.push(element);
      else noParArray.push(element);
    });
    let recorrido = [];

    parArray.map((element, index) => {
      recorrido.push([parArray[index], noParArray[index]]);
    });

    console.log(recorrido);

    var linea = {
      type: "LineString",
      coordinates: recorrido,
    };

    const body = {
      recorrido: linea,
    };

    const settings = {
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
      body: JSON.stringify(body),
    };

    console.log(body);
    const url =
      "http://localhost:4567/lineas/editar/" + localStorage["idLinea"];
    console.log(settings);
    fetch(url, settings)
      .then((res) => {
        console.log("Res url: " + url);
        console.log(res);
      })
      .then((data) => {
        console.log(data);
      });

    var modal = new Modal(document.getElementById("successModal"), {});
    modal.show();
  }
  render() {
    return (
      <div>
        <div id="map" className="map"></div>
        <div className="position-fixed bottom-0 end-0 p-3">
          <div
            id="toastMessage"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
            className="toast"
            data-autohide="false"
          >
            <div className="toast-body">
              Haga clic sobre la linea y luego edite su recorrido. Al terminar
              haga clic en confirmar
              <div className="mt-2 pt-2 border-top d-grid gap-2">
                <button
                  type="button"
                  className="btn btn-success"
                  onClick={this.confirmarCambios}
                >
                  Confirmar
                </button>
                <Link to={"/home"} className="link btn btn-danger btn-sm">
                  Salir
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="successModal"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content text-center">
              <div class="modal-header">
                <h5 class="modal-title ">L&iacute;nea Modificada</h5>
              </div>
              <div className="modal-body d-grid gap-2 text-center">
                <p>Se modific&oacute; la l&iacute;nea correctamente</p>

                <a
                  onClick={() => (window.location.href = "/home")}
                  className="link btn btn-success"
                >
                  Volver al inicio
                </a>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="successModal"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">L&iacute;nea Modificada</h5>
              </div>
              <div className="modal-body d-grid gap-2 text-center">
                <p>Se modific&oacute; la l&iacute;nea correctamente</p>

                <Link to={"/home"} className="link btn btn-success btn-sm">
                  Vovler al inicio
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditarLinea;
