import React from "react";
import logo from "../../favicon.ico";
import "./Loading.css";

class Loading extends React.Component {
  render() {
    return (
      <>
        <div class="position-absolute top-50 start-50 translate-middle ">
          <img
            src={logo}
            class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}"
          ></img>
          <h2 class="text-yellow text-center">Cargando p&aacute;gina...</h2>
        </div>
      </>
    );
  }
}

export default Loading;
