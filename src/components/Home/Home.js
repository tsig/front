import React, { useEffect } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import View from "ol/View";
import * as olProj from "ol/proj";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import VectorSource from "ol/source/Vector";
import { Circle as CircleStyle, Fill, Icon, Stroke, Style } from "ol/style";
import { Vector as VectorLayer } from "ol/layer";
import TileLayer from "ol/layer/Tile";
import TileWMS from "ol/source/TileWMS";
import OSM from "ol/source/OSM";
import { Modal } from "bootstrap";
import { Link } from "react-router-dom";
import { fromLonLat, Projection } from "ol/proj";
import LineString from "ol/geom/LineString";
import { Draw, Modify, Snap } from "ol/interaction";
import { GeoJSON } from "ol/format";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      idParada: 0,
      idLinea: 0,
      codigoLinea: 0,
      idHorario: 0,
      cordx: 0,
      cordy: 0,
      showModalSearchAddress: props.showModalSearchAddress,
      showModalSearchLine: props.showModalSearchLine,
      filterParadas: props.filterParadas,
      drawLinesPolygon: props.drawLinesPolygon,
      toggleShowModalSearchAddress: props.toggleShowModalSearchAddress,
      toggleShowModalSearchLine: props.toggleShowModalSearchLine,
      addressStreet: "",
      addressNumber: "",
      lineCode: "",
      lineDestination: "",
      contentLineasParada: [],
      contentHorarios: [],
      mapLayers: [],
      map: null,
      valueSelectParadaAAgregar: null,
      valueSelectHorarioAAgregar: null,
    };

    this.closeModalSearchLine = this.closeModalSearchLine.bind(this);
    this.closeModalSearchAddress = this.closeModalSearchAddress.bind(this);
    this.onChangeAddressNumber = this.onChangeAddressNumber.bind(this);
    this.onChangeAddressStreet = this.onChangeAddressStreet.bind(this);
    this.onChangeLineCode = this.onChangeLineCode.bind(this);
    this.onChangeLineDestination = this.onChangeLineDestination.bind(this);
    this.fetchSearchAddress = this.fetchSearchAddress.bind(this);
    this.fetchSearchLine = this.fetchSearchLine.bind(this);
    this.buildLineasParada = this.buildLineasParada.bind(this);
    this.desasociarLinea = this.desasociarLinea.bind(this);
    this.deleteParada = this.deleteParada.bind(this);
    this.deleteLinea = this.deleteLinea.bind(this);
    this.deleteHorario = this.deleteHorario.bind(this);
    this.updateHorario = this.updateHorario.bind(this);
    this.handleChangeParadaAAgregar =
      this.handleChangeParadaAAgregar.bind(this);
    this.agregarLineaAParada = this.agregarLineaAParada.bind(this);
    this.agregarHorario = this.agregarHorario.bind(this);
    this.fetchAgregarLineaAParada = this.fetchAgregarLineaAParada.bind(this);
    this.showHorariosDeLinea = this.showHorariosDeLinea.bind(this);
    this.handleChangeSelectHorarioAAgregar =
      this.handleChangeSelectHorarioAAgregar.bind(this);
    this.handleChangeSelectHorarioAModificar =
      this.handleChangeSelectHorarioAModificar.bind(this);
    this.fetchUpdateHorario = this.fetchUpdateHorario.bind(this);
  }

  hideOrShowModal(modalId, showFlag) {
    let modal = new Modal(document.getElementById(modalId), {});
    if (showFlag) {
      console.log("el flag de " + modalId + " esta en " + showFlag);
      modal.show();
    } else {
      modal.hide();
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.searchParadas(this.state.filterParadas);
    this.state.map.removeLayer("tsig:parada");
    this.hideOrShowModal(
      "modal-search-address",
      this.state.showModalSearchAddress
    );
    this.hideOrShowModal("modal-search-line", this.state.showModalSearchLine);
  }

  currentPositionStyle() {
    return new Style({
      image: new Icon({
        anchor: [0.5, 1],
        anchorXUnits: "fraction",
        anchorYUnits: "fraction",
        opacity: 1,
        scale: 0.09,
        src: "https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png",
      }),
    });
  }

  buildCurrentPositionLayer(feature) {
    return new VectorLayer({
      source: new VectorSource({
        features: [feature],
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 46],
          anchorXUnits: "fraction",
          anchorYUnits: "pixels",
          opacity: 0.95,
          src: "data/icon.png",
        }),
        stroke: new Stroke({
          width: 3,
          color: [255, 0, 0, 1],
        }),
        fill: new Fill({
          color: [0, 0, 255, 0.6],
        }),
      }),
    });
  }

  fetchSearchLine() {
    let lineSearch = new TileLayer({
      visible: true,
      source: new TileWMS({
        url: "http://localhost:8080/geoserver/tsig/wms",
        serverType: "geoserver",
        params: {
          TILED: true,
          LAYERS: "tsig:linea",
          cql_filter:
            "codigo=" +
            this.state.lineCode +
            " AND destino='" +
            this.state.lineDestination +
            "'",
          styles: "lineasearch",
        },
      }),
    });

    this.state.map.addLayer(lineSearch);
  }

  fetchSearchLineasPoligono(filter) {
    let lineSearch = new TileLayer({
      visible: true,
      source: new TileWMS({
        url: "http://localhost:8080/geoserver/tsig/wms",
        serverType: "geoserver",
        params: {
          TILED: true,
          LAYERS: "tsig:linea",
          cql_filter: filter,
          styles: "lineasearch",
        },
      }),
    });

    this.state.map.addLayer(lineSearch);
  }

  searchParadas(value) {
    let paradaSearch = new TileLayer({
      id: "paradas",
      visible: true,
      source: new TileWMS({
        url: "http://localhost:8080/geoserver/tsig/wms",
        params: {
          FORMAT: "image/png",
          VERSION: "1.1.1",
          tiled: true,
          LAYERS: "tsig:parada",
          cql_filter: "estado = " + value,
        },
      }),
    });

    this.state.map.addLayer(paradaSearch);
  }

  fetchSearchAddress() {
    let addressStreet = this.state.addressStreet;
    let addressNumber = this.state.addressNumber;
    let urlAddress =
      "https://direcciones.ide.uy/api/v0/geocode/BusquedaDireccion?calle=" +
      addressStreet +
      "%20" +
      addressNumber +
      "&departamento=montevideo&localidad=montevideo";

    fetch(urlAddress)
      .then((response) => response.json())
      .then((data) => {
        let address = data[0];
        let posAddress = olProj.fromLonLat([address.puntoX, address.puntoY]);
        let addressFeature = new Feature({
          geometry: new Point(posAddress),
        });

        addressFeature.setStyle(this.currentPositionStyle());
        this.state.map.addLayer(this.buildCurrentPositionLayer(addressFeature));
        this.state.map.setView(
          new View({
            center: posAddress,
            zoom: 16,
          })
        );
      });
  }

  // uso componentDidMount para que corra este script luego de cargar el div de mapa que esta debajo de esto
  componentDidMount() {
    const getParadas = async (idParada) => {
      var url_lineas_de_parada =
        "http://localhost:4567/paradas/" + idParada + "/lineas";

      var settings_lineas_de_parada = {
        method: "GET",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
        cache: "no-cache",
      };

      const request = await fetch(
        url_lineas_de_parada,
        settings_lineas_de_parada
      );
      const response = request.json();
      return response;
    };

    let currentComponent = this;
    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);

    var iconFeature = new Feature({
      geometry: new Point(pos),
    });

    var iconStyle = new Style({
      image: new Icon({
        anchor: [0.5, 1],
        anchorXUnits: "fraction",
        anchorYUnits: "fraction",
        opacity: 1,
        scale: 0.09,
        src: "https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678111-map-marker-512.png",
      }),
    });

    iconFeature.setStyle(iconStyle);

    var miPosicion = new VectorLayer({
      source: new VectorSource({
        features: [iconFeature],
      }),
      style: new Style({
        image: new Icon({
          anchor: [0.5, 46],
          anchorXUnits: "fraction",
          anchorYUnits: "pixels",
          opacity: 0.95,
          src: "data/icon.png",
        }),
        stroke: new Stroke({
          width: 3,
          color: [255, 0, 0, 1],
        }),
        fill: new Fill({
          color: [0, 0, 255, 0.6],
        }),
      }),
    });

    var format = "image/png";

    var lineas = new TileLayer({
      id: "lineas",
      visible: true,
      source: new TileWMS({
        url: "http://localhost:8080/geoserver/tsig/wms",
        serverType: "geoserver",
        params: {
          TILED: true,
          LAYERS: "tsig:linea",
        },
      }),
    });

    var mapa = new TileLayer({
      source: new OSM(),
    });

    var sourceDraw = new VectorSource();

    var vectorDraw = new VectorLayer({
      source: sourceDraw,
      style: new Style({
        fill: new Fill({
          color: "rgba(255, 255, 255, 0.2)",
        }),
        stroke: new Stroke({
          color: "#ffcc33",
          width: 2,
        }),
        image: new CircleStyle({
          radius: 7,
          fill: new Fill({
            color: "#ffcc33",
          }),
        }),
      }),
    });

    const layers = [mapa, vectorDraw, lineas, miPosicion];
    this.setState({ mapLayers: layers });

    var view = new View({
      //center: [-6251681.1746235965, -4148062.303116895], // centro en montevideo
      center: pos,
      zoom: 16,
    });

    var map = new Map({
      layers: layers,
      target: "map",
      view: view,
    });

    var modify = new Modify({ source: sourceDraw });
    map.addInteraction(modify);

    var draw, snap; // global so we can remove them later
    var typeSelect = "Polygon";

    draw = new Draw({
      source: sourceDraw,
      type: typeSelect,
    });

    if (this.state.drawLinesPolygon) {
      map.addInteraction(draw);
      snap = new Snap({ source: sourceDraw });
      map.addInteraction(snap);
    }

    var selfs = this;

    draw.on("drawend", function (e2) {
      var feature = e2.feature;
      var geoJsonPolygon = new GeoJSON().writeFeature(feature);
      console.log(JSON.parse(geoJsonPolygon));

      let requestPolygon = JSON.parse(geoJsonPolygon);

      const url = "http://localhost:4567/lineas/poligono/";

      const settings = {
        method: "POST",
        headers: {
          Accept: "application/json, text/plain, */*",
          "Content-Type": "application/json",
        },
        cache: "no-cache",
        body: JSON.stringify(requestPolygon),
      };

      fetch(url, settings)
        .then((response) => response.json())
        .then((data) => {
          let cqlFilter = "id IN (" + data.join() + ")";
          selfs.fetchSearchLineasPoligono(cqlFilter);
        });

      map.removeInteraction(draw);
    });

    this.setState({ map: map });

    let self = this;
    map.on("singleclick", function (evt) {
      document.getElementById("m-estado-parada").innerHTML = "";
      document.getElementById("m-nombre-parada").innerHTML = "";
      var viewResolution = /** @type {number} */ (view.getResolution());

      let layers = self.state.map.getLayers();
      let sourceParada = layers.array_
        .filter((layer) => layer.values_.id === "paradas")[0]
        .getSource();
      var sourceLinea = lineas.getSource();
      var urlLinea = sourceLinea.getFeatureInfoUrl(
        evt.coordinate,
        viewResolution,
        view.getProjection(),
        { INFO_FORMAT: "application/json", FEATURE_COUNT: 50 }
      );
      var urlParada = sourceParada.getFeatureInfoUrl(
        evt.coordinate,
        viewResolution,
        view.getProjection(),
        { INFO_FORMAT: "application/json", FEATURE_COUNT: 50 }
      );

      //hacer secuencial , primero parada y luego linea.
      var tieneParada = false;
      if (urlParada) {
        console.log("estoy en parada");
        fetch(urlParada)
          .then(function (response) {
            console.log(response);
            return response.text();
          })
          .then(function (html) {
            var data = JSON.parse(html);
            console.log("parada");
            console.log(data);
            tieneParada = false;
            if (data.features.length > 0) {
              tieneParada = true;
              var id = data.features[0].id;
              currentComponent.setState({
                idParada: id,
                cordx: evt.coordinate[0],
                cordy: evt.coordinate[1],
              });
              var estado = data.features[0].properties.estado;
              var nombre = data.features[0].properties.nombre;

              estado = estado === true ? "Habilitada" : "Deshabilitada";

              document
                .getElementById("m-estado-parada")
                .classList.remove("parada-habilitada");
              document
                .getElementById("m-estado-parada")
                .classList.remove("parada-deshabilitada");

              if (estado === "Habilitada") {
                document
                  .getElementById("m-estado-parada")
                  .classList.add("parada-habilitada");
              } else {
                document
                  .getElementById("m-estado-parada")
                  .classList.add("parada-deshabilitada");
              }
              console.log("Nombre: " + nombre);
              console.log("Estado: " + estado);

              document.getElementById("m-estado-parada").innerHTML = estado;
              document.getElementById("m-nombre-parada").innerHTML = nombre;

              // ahora busco las lineas de esta parada:
              const idParada = id.replace("parada.", "");
              const idLinea = id.replace("linea.", "");
              var dataParada = getParadas(idParada);
              dataParada.then((data) => {
                //dibujamos la tabla de lineas en el modal parada
                console.log(data);

                var row = "";
                if (localStorage["isLogged"] == "true") {
                  document.getElementById("lineas_para_agregar").innerHTML = "";
                }
                document.getElementById("lineas-de-parada").innerHTML = "";
                if (data.length === 0) {
                  document.getElementById("lineas-de-parada").innerHTML = "";
                  document.getElementById("btn-agregar-linea").disabled = true;
                } else {
                  document.getElementById("lineas-de-parada").innerHTML = "";
                  var contadorDeLineasDes = self.buildLineasParada(
                    data,
                    idParada
                  );

                  if (localStorage["isLogged"] == "true") {
                    if (contadorDeLineasDes == 0) {
                      // incialilzo el option de las lineas que estan desasociadas
                      document.getElementById(
                        "btn-agregar-linea"
                      ).disabled = true;
                    } else {
                      document.getElementById(
                        "btn-agregar-linea"
                      ).disabled = false;
                    }
                  }
                }
              });

              var modal = new Modal(
                document.getElementById("modal-parada"),
                {}
              );
              modal.show();
            } else {
              console.log("Hace clic en una parada o linea gil");
            }
          });
      }
      if (urlLinea) {
        console.log("estoy en linea");
        fetch(urlLinea)
          .then(function (response) {
            console.log(response);
            return response.text();
          })
          .then(function (html) {
            var data = JSON.parse(html);
            console.log("linea");
            console.log(data);
            console.log(tieneParada);
            if (data.features.length > 0 && !tieneParada) {
              var id = data.features[0].id;

              var codigoLinea = data.features[0].properties.codigo;
              console.log(codigoLinea);
              currentComponent.setState({
                idLinea: id,
                codigoLinea: codigoLinea,
                //cordx: evt.coordinate[0],
                // cordy: evt.coordinate[1],
              });
              console.log("Muestro modal linea");

              var codigo = data.features[0].properties.codigo;
              document.getElementById("m-codigo-linea").innerHTML = codigo;
              var empresa = data.features[0].properties.empresa;
              document.getElementById("m-empresa-linea").innerHTML = empresa;
              var origen = data.features[0].properties.origen;
              document.getElementById("m-origen-linea").innerHTML = origen;
              var destino = data.features[0].properties.destino;
              document.getElementById("m-destino-linea").innerHTML = destino;

              var modal = new Modal(document.getElementById("modal-linea"), {});
              modal.show();
            } else {
              console.log("No muestro modal linea");
            }
          });
      }
    });
  }

  onChangeAddressNumber(event) {
    this.setState({ addressNumber: event.target.value });
  }

  onChangeAddressStreet(event) {
    this.setState({ addressStreet: event.target.value });
  }

  onChangeLineCode(event) {
    this.setState({ lineCode: event.target.value });
  }

  onChangeLineDestination(event) {
    this.setState({ lineDestination: event.target.value });
  }

  closeModalSearchAddress() {
    this.state.toggleShowModalSearchAddress();
  }

  closeModalSearchLine() {
    this.state.toggleShowModalSearchLine();
  }

  fetchdesasociarLinea(idParada, idLinea) {
    console.log("fetchdesasociarLinea");
    const url =
      "http://localhost:4567/paradas/" + idParada + "/linea/" + idLinea;

    const settings = {
      method: "DELETE",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
    };
    fetch(url, settings).then(function (response) {
      console.log(response);
      if (response.status == 200) {
        console.log("Todo ok");

        return true;
      } else {
        return false;
      }
    });
  }

  fetchDeleteParada(idParada) {
    const url = "http://localhost:4567/paradas/" + idParada;

    const settings = {
      method: "DELETE",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
    };
    fetch(url, settings).then(function (response) {
      console.log(response);
      if (response.status == 200) {
        console.log("Todo ok");

        return true;
      } else {
        return false;
      }
    });
  }

  fetchDeleteLinea(idLinea) {
    const url = "http://localhost:4567/lineas/" + idLinea;

    const settings = {
      method: "DELETE",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
    };
    fetch(url, settings).then(function (response) {
      console.log(response);
      if (response.status == 200) {
        console.log("Todo ok");

        return true;
      } else {
        return false;
      }
    });
  }

  fetchDeleteHorario(idHorario) {
    const url = "http://localhost:4567/horario/delete/" + idHorario;
    console.log("muestro la puta URL");
    console.log(url);

    const settings = {
      method: "DELETE",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
    };
    fetch(url, settings).then(function (response) {
      console.log(response);
      if (response.status == 200) {
        console.log("Todo ok");

        return true;
      } else {
        return false;
      }
    });
  }

  desasociarLinea(event) {
    console.log("desasociarLinea");
    let idLinea = event.target.getAttribute("idLinea");
    let idParada = this.state.idParada.replace("parada.", "");
    var borrado = this.fetchdesasociarLinea(idParada, idLinea);
    if (borrado) {
    } else {
      var mje = document.getElementById("mensajeOK-linea");

      if (mje.classList.contains("hide")) {
        mje.classList.remove("hide");
        mje.classList.add("show");
      }
    }
    window.location.reload(false);
  }

  deleteParada(event) {
    let idParada = this.state.idParada.replace("parada.", "");
    var borrado = this.fetchDeleteParada(idParada);
    if (borrado) {
    } else {
      var mje = document.getElementById("mensajeOK");

      if (mje.classList.contains("hide")) {
        mje.classList.remove("hide");
        mje.classList.add("show");
      }
    }
    window.location.reload(false);
  }

  deleteLinea(event) {
    let idLinea = this.state.idLinea.toString().replace("linea.", "");
    var borrado = this.fetchDeleteLinea(idLinea);
    if (borrado) {
    } else {
      var mje = document.getElementById("mensajeOK");

      if (mje.classList.contains("hide")) {
        mje.classList.remove("hide");
        mje.classList.add("show");
      }
    }
    window.location.reload(false);
  }

  deleteHorario(event) {
    let idHorario = event.target.getAttribute("idHorario");
    console.log("muestro el  IDhorario");
    console.log(idHorario);
    var borrado = this.fetchDeleteHorario(idHorario);
    if (borrado) {
    } else {
      var mje = document.getElementById("mensajeOK");

      if (mje.classList.contains("hide")) {
        mje.classList.remove("hide");
        mje.classList.add("show");
      }
    }
    window.location.reload(false);

    console.log("borro horario");
  }
  updateHorario(event) {
    console.log("actualizo horario");
    let idHorario = event.target.getAttribute("idHorario");
    this.setState({ idHorario: idHorario });
    console.log(idHorario);
    this.fetchUpdateHorario(idHorario);
  }

  showHorariosDeLinea(event) {
    var modal = new Modal(document.getElementById("modal-horarios-linea"), {});
    // guardo en state el idLinea
    let idLinea = event.target.getAttribute("idLinea");
    this.setState({ idLinea: idLinea });

    // busco los horarios de esta parada-linea y genero los rows de horarios
    this.fetchGetHorarios(idLinea);
    modal.show();
  }

  //ROWS de la TABLA
  buildLineasParada(data, idParada) {
    var contadorDeLineasDes = 0;
    data.forEach((d) => {
      if (d.asociada) {
        if (localStorage["isLogged"] == "true") {
          let idTr = "linea-" + d.id;
          let row = (
            <tr id={idTr}>
              <td>{d.codigo}</td>
              <td>{d.origen}</td>
              <td>{d.destino}</td>
              <td>{d.empresa}</td>
              <td>
                <button
                  type="button"
                  className="btn btn-primary"
                  idLinea={d.id}
                  idParada={idParada}
                  onClick={this.showHorariosDeLinea}
                >
                  Ver Horarios
                </button>
              </td>
              <td>
                <button
                  type="button"
                  className="btn btn-danger"
                  idLinea={d.id}
                  onClick={this.desasociarLinea}
                >
                  Borrar
                </button>
              </td>
            </tr>
          );

          let newlist = this.state.contentLineasParada;
          newlist.push(row);

          this.setState({ contentLineasParada: newlist });
        } else {
          let row = (
            <tr>
              <td>{d.codigo}</td>
              <td>{d.origen}</td>
              <td>{d.destino}</td>
              <td>{d.empresa}</td>
              <td>
                <button
                  type="button"
                  className="btn btn-primary"
                  idLinea={d.id}
                  idParada={idParada}
                  onClick={this.showHorariosDeLinea}
                >
                  Ver Horarios
                </button>
              </td>
            </tr>
          );

          let newlist = this.state.contentLineasParada;
          newlist.push(row);

          this.setState({ contentLineasParada: newlist });
        }
      } else {
        contadorDeLineasDes++;
        if (localStorage["isLogged"] == "true") {
          var sel = document.getElementById("lineas_para_agregar");
          var opt = document.createElement("option");
          var txt = "[" + d.codigo + "] " + d.origen + " a " + d.destino;
          opt.appendChild(document.createTextNode(txt));
          opt.value = d.id;
          if (contadorDeLineasDes === 1) {
            this.setState({ valueSelectParadaAAgregar: d.id });
          }
          sel.appendChild(opt);
        }
      }
    });

    return contadorDeLineasDes;
  }

  fetchUpdateHorario = async (idHorario) => {
    let horario = this.state.valueSelectHorarioAModificar;

    const url = "http://localhost:4567/horario/update/" + idHorario;
    console.log(url);

    const body = horario;

    const settings = {
      method: "PUT",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
      body: JSON.stringify(body),
    };

    const request = await fetch(url, settings)
      .then((response) => {
        return response;
      })
      .then((data) => {
        console.log(data);
        return data;
      });
    window.location.reload(false);
  };

  fetchGetHorarios = (idLinea) => {
    let idParada = this.state.idParada.replace("parada.", "");
    var url_horarios_de_parada =
      "http://localhost:4567/horario/getHorario/" + idParada + "/" + idLinea;
    console.log(url_horarios_de_parada);

    var settings_horarios_de_parada = {
      method: "GET",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
    };

    this.state.contentHorarios = []; // reinicio
    const request = fetch(url_horarios_de_parada, settings_horarios_de_parada)
      .then((response) => response.json())
      .then((data) => {
        this.buildHorarios(data);
      });
  };
  buildHorarios(data) {
    var contadorDeLineasDes = 0;
    console.log(data);
    data.forEach((d) => {
      if (localStorage["isLogged"] == "true") {
        let row = (
          <tr>
            <td>{d.horario}</td>
            <td>
              <input
                value={this.valueHorarioAModificar}
                onChange={this.handleChangeSelectHorarioAModificar}
                type="text"
                class="form-control"
                id="modificarHorario"
              ></input>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-primary"
                idHorario={d.idHorario}
                onClick={this.updateHorario}
              >
                Modificar
              </button>
            </td>
            <td>
              <button
                type="button"
                className="btn btn-danger"
                idHorario={d.idHorario}
                onClick={this.deleteHorario} // methodo a hacer
              >
                Borrar
              </button>
            </td>
          </tr>
        );

        let newlist = this.state.contentHorarios;
        newlist.push(row);

        this.setState({ contentHorarios: newlist });
      } else {
        let row = (
          <tr>
            <td>{d.horario}</td>
          </tr>
        );

        let newlist = this.state.contentHorarios;
        newlist.push(row);

        this.setState({ contentHorarios: newlist });
      }
      /* else {
        contadorDeLineasDes++;
        if (localStorage["isLogged"] == "true") {
          var sel = document.getElementById("horarios_para_agregar");
          var opt = document.createElement("option");
          var txt = "[" + d.codigo + "] " + d.origen + " a " + d.destino;
          opt.appendChild(document.createTextNode(txt));
          opt.value = d.id;
          if (contadorDeLineasDes === 1) {
            this.setState({ valueSelectHorarioAAgregar: d.id });
          }
          sel.appendChild(opt);
        }
      }*/
    });

    return contadorDeLineasDes;
  }

  handleChangeParadaAAgregar(event) {
    this.setState({ valueSelectParadaAAgregar: event.target.value });
  }

  handleChangeSelectHorarioAAgregar(event) {
    this.setState({ valueSelectHorarioAAgregar: event.target.value });
  }
  handleChangeSelectHorarioAModificar(event) {
    this.setState({ valueSelectHorarioAModificar: event.target.value });
  }

  fetchAgregarLineaAParada(idLinea) {
    let idParada = this.state.idParada.replace("parada.", "");
    const url =
      "http://localhost:4567/paradas/" +
      idParada +
      "/addLineaToParada/" +
      idLinea;

    const settings = {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      cache: "no-cache",
    };
    fetch(url, settings).then(function (response) {
      if (response.status == 200) {
        console.log("Todo ok");
        return true;
      } else {
        return false;
      }
    });
    window.location.reload(false);
  }

  agregarLineaAParada = () => {
    this.fetchAgregarLineaAParada(this.state.valueSelectParadaAAgregar);
  };

  //NUEVO

  fetchAgregarHorario() {
    const url = "http://localhost:4567/horario/add";

    console.log("agrego un horario");

    const body = {
      idLinea: this.state.idLinea,
      idParada: this.state.idParada.replace("parada.", ""),
      horario: this.state.valueSelectHorarioAAgregar,
      habilitado: true,
    };

    console.log(body);

    const settings = {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
      cache: "no-cache",
    };
    fetch(url, settings).then(function (response) {
      if (response.status == 200) {
        console.log(response);
        return true;
      } else {
        console.log("todo mal");
        return false;
      }
    });
    window.location.reload(false);
  }

  agregarHorario = () => {
    console.log("agregar horario");
    this.fetchAgregarHorario();
  };

  render() {
    const isLoggedIn = localStorage["isLogged"];
    let buttonEdit,
      buttonEditLinea,
      buttonBorrar,
      tableLineasDeParada,
      agregarLineasAParada,
      agregarHorario,
      tableHorarios,
      buttonBorrarLinea;

    if (isLoggedIn == "true") {
      //BOTON EDITAR

      buttonBorrar = (
        <button
          type="button"
          className="btn btn-danger"
          id="btn-borrar-parada"
          onClick={this.deleteParada}
        >
          Borrar Parada
        </button>
      );
      buttonBorrarLinea = (
        <button
          type="button"
          className="btn btn-danger"
          id="btn-borrar-parada"
          onClick={this.deleteLinea}
        >
          Borrar Linea
        </button>
      );
      buttonEdit = (
        <Link
          to={`EditarParada?id=${this.state.idParada}&x=${this.state.cordx}&y=${this.state.cordy}`}
          className="link btn btn-primary"
        >
          Editar Ubicacion
        </Link>
      );
      buttonEditLinea = (
        <Link
          to={`EditarLinea?codigo=${this.state.codigoLinea}&id=${this.state.idLinea}`}
          className="link btn btn-primary"
        >
          Editar Recorrido
        </Link>
      );

      //TABLA
      tableLineasDeParada = (
        <table className="table table-light table-hover">
          <thead>
            <tr>
              <th scope="col">C&oacute;digo</th>
              <th scope="col">Origen</th>
              <th scope="col">Destino</th>
              <th scope="col">Empresa</th>
              <th scope="col">Horarios</th>
              <th scope="col">Eliminar</th>
            </tr>
          </thead>
          <tbody id="lineas-de-parada">
            {this.state.contentLineasParada.map((row) => {
              return row;
            })}
          </tbody>
        </table>
      );
      if (this.state.contentHorarios.length === 0) {
        tableHorarios = (
          <div
            class="alert alert-warning alert-dismissible fade show"
            role="alert"
            id="mensajeOK"
          >
            Esta parada no tiene horarios asociados.
          </div>
        );
      } else {
        tableHorarios = (
          <table className="table table-light table-hover">
            <thead>
              <tr>
                <th scope="col">Horario</th>
                <th scope="col" colspan="2">
                  Modificar
                </th>
                <th scope="col">Eliminar</th>
              </tr>
            </thead>
            <tbody id="horarios-de-linea">
              {this.state.contentHorarios.map((row) => {
                return row;
              })}
            </tbody>
          </table>
        );
      }

      //BOTON AGREGAR
      agregarLineasAParada = (
        <div className="col-12">
          <select
            value={this.state.valueSelectParadaAAgregar}
            onChange={this.handleChangeParadaAAgregar}
            class="form-select"
            id="lineas_para_agregar"
          >
            <option selected>L&iacute;neas sin asignar</option>
          </select>
          <button
            type="button"
            className="btn btn-success btn mt-3 btn-block"
            id="btn-agregar-linea"
            onClick={this.agregarLineaAParada}
          >
            Agregar Linea a Parada
          </button>
        </div>
      );

      agregarHorario = (
        <div className="col-12">
          <input
            value={this.state.valueSelectHorarioAAgregar}
            onChange={this.handleChangeSelectHorarioAAgregar}
            type="text"
            class="form-control"
            id="horarios_para_agregar"
          ></input>
          <button
            type="button"
            className="btn btn-success btn mt-3 btn-block"
            id="btn-agregar-horario"
            onClick={this.agregarHorario}
          >
            Agregar Horario
          </button>
        </div>
      );
    } else {
      buttonBorrar = "";
      buttonEdit = "";
      buttonEditLinea = "";
      buttonBorrarLinea = "";
      tableLineasDeParada = (
        <table className="table table-light table-hover">
          <thead>
            <tr>
              <th scope="col">C&oacute;digo</th>
              <th scope="col">Origen</th>
              <th scope="col">Destino</th>
              <th scope="col">Empresa</th>
              <th scope="col">Horarios</th>
            </tr>
          </thead>
          <tbody id="lineas-de-parada">
            {this.state.contentLineasParada.map((row) => {
              return row;
            })}
          </tbody>
        </table>
      );
      tableHorarios = (
        <table className="table table-light table-hover">
          <thead>
            <tr>
              <th scope="col">Horario</th>
            </tr>
          </thead>
          <tbody id="horarios-de-linea">
            {this.state.contentHorarios.map((row) => {
              return row;
            })}
          </tbody>
        </table>
      );
      agregarHorario = "";
    }

    return (
      <div>
        <div id="map" className="map"></div>;
        <div
          className="modal fade"
          id="modal-parada"
          tabindex="-1"
          aria-labelledby="modal-parada"
          aria-hidden="true"
          data-backdrop="false"
        >
          <div className="modal-dialog modal-dialog-centered modal-xl">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Datos de la Parada
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body text-center" id="data-parada">
                <div className="row g-3">
                  <div className="col-6 ">
                    <div className="card">
                      <p>
                        <b>Nombre:</b>
                        <br />
                        <span id="m-nombre-parada"></span>
                      </p>
                    </div>
                  </div>
                  <div className="col-6">
                    <div className="card">
                      <p>
                        <b>Estado:</b> <br />
                        <span id="m-estado-parada"></span>
                      </p>
                    </div>
                  </div>
                  {agregarLineasAParada}
                  <div className="col-12 table-responsive">
                    {tableLineasDeParada}
                    <div
                      class="alert alert-success alert-dismissible fade hide"
                      role="alert"
                      id="mensajeOK"
                    >
                      Parada borrada correctamente
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Cerrar
                </button>
                {buttonEdit}
                {buttonBorrar}
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="modal-linea"
          tabindex="-1"
          aria-labelledby="modal-linea"
          aria-hidden="true"
          data-backdrop="false"
        >
          <div className="modal-dialog modal-dialog-centered modal-xl">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Datos de la Linea
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body text-center" id="data-parada">
                <div className="row g-3">
                  <div className="col-6 ">
                    <div className="card">
                      <p>
                        <b>C&oacute;digo:</b>
                        <br />
                        <span id="m-codigo-linea"></span>
                      </p>
                    </div>
                  </div>
                  <div className="col-6 ">
                    <div className="card">
                      <p>
                        <b>Empresa:</b>
                        <br />
                        <span id="m-empresa-linea"></span>
                      </p>
                    </div>
                  </div>
                  <div className="col-6 ">
                    <div className="card">
                      <p>
                        <b>Origen:</b>
                        <br />
                        <span id="m-origen-linea"></span>
                      </p>
                    </div>
                  </div>
                  <div className="col-6 ">
                    <div className="card">
                      <p>
                        <b>Destino:</b>
                        <br />
                        <span id="m-destino-linea"></span>
                      </p>
                    </div>
                  </div>
                  <div className="col-12 table-responsive">
                    <div
                      class="alert alert-success alert-dismissible fade hide"
                      role="alert"
                      id="mensajeOK-linea"
                    >
                      Linea borrada correctamente
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Cerrar
                </button>
                {buttonEditLinea}
                {buttonBorrarLinea}
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="modal-horarios-linea"
          tabindex="-1"
          aria-labelledby="modal-horarios-linea"
          aria-hidden="true"
          data-backdrop="false"
        >
          <div className="modal-dialog modal-dialog-centered modal-md">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Horarios
                </h5>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                ></button>
              </div>
              <div className="modal-body text-center" id="data-parada">
                <div className="row g-3">
                  {agregarHorario}
                  <div className="col-12 table-responsive">
                    {tableHorarios}
                    <div
                      class="alert alert-success alert-dismissible fade hide"
                      role="alert"
                      id="mensajeOK"
                    >
                      Horario Agregado
                    </div>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-bs-dismiss="modal"
                >
                  Cerrar
                </button>
              </div>
            </div>
          </div>
        </div>
        <ModalSearchAddress
          fetchSearchAddress={this.fetchSearchAddress}
          onChangeAddressNumber={this.onChangeAddressNumber}
          onChangeAddressStreet={this.onChangeAddressStreet}
          closeModalSearchAddress={this.closeModalSearchAddress}
        />
        <ModalSearchLine
          fetchSearchLine={this.fetchSearchLine}
          onChangeLineCode={this.onChangeLineCode}
          onChangeLineDestination={this.onChangeLineDestination}
          closeModalSearchLine={this.closeModalSearchLine}
        />
      </div>
    );
  }
}

const ModalSearchAddress = (props) => (
  <div
    className="modal fade"
    id="modal-search-address"
    tabIndex="-1"
    aria-labelledby="modal-search-address"
    aria-hidden="true"
    data-backdrop="false"
  >
    <div className="modal-dialog modal-dialog-centered">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">
            Buscar una dirección
          </h5>
          <button
            type="button"
            className="btn-close"
            data-bs-dismiss="modal"
            aria-label="Close"
          />
        </div>
        <div className="modal-body">
          <form>
            <div className="mb-3">
              <label htmlFor="" className="form-label">
                Calle
              </label>
              <input
                type="text"
                className="form-control"
                id="calle"
                onBlur={props.onChangeAddressStreet}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="origen" className="form-label">
                Numero
              </label>
              <input
                type="text"
                className="form-control"
                id="origen"
                onBlur={props.onChangeAddressNumber}
              />
            </div>
          </form>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-success"
            data-bs-dismiss="modal"
            onClick={props.fetchSearchAddress}
          >
            Buscar
          </button>
          <button
            type="button"
            className="btn btn-secondary"
            data-bs-dismiss="modal"
            onClick={props.closeModalSearchAddress}
          >
            Cerrar
          </button>
        </div>
      </div>
    </div>
  </div>
);

const ModalSearchLine = (props) => (
  <div
    className="modal fade"
    id="modal-search-line"
    tabIndex="-1"
    aria-labelledby="modal-search-line"
    aria-hidden="true"
    data-backdrop="false"
  >
    <div className="modal-dialog modal-dialog-centered">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title" id="exampleModalLabel">
            Buscar una linea por código y destino
          </h5>
          <button
            type="button"
            className="btn-close"
            data-bs-dismiss="modal"
            aria-label="Close"
          />
        </div>
        <div className="modal-body">
          <form>
            <div className="mb-3">
              <label htmlFor="" className="form-label">
                Código
              </label>
              <input
                type="text"
                className="form-control"
                id="calle"
                onBlur={props.onChangeLineCode}
              />
            </div>
            <div className="mb-3">
              <label htmlFor="origen" className="form-label">
                Destino
              </label>
              <input
                type="text"
                className="form-control"
                id="origen"
                onBlur={props.onChangeLineDestination}
              />
            </div>
          </form>
        </div>
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-success"
            data-bs-dismiss="modal"
            onClick={props.fetchSearchLine}
          >
            Buscar
          </button>
          <button
            type="button"
            className="btn btn-secondary"
            data-bs-dismiss="modal"
            onClick={props.closeModalSearchLine}
          >
            Cerrar
          </button>
        </div>
      </div>
    </div>
  </div>
);

export default Home;
