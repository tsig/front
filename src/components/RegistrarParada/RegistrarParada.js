import React, { useEffect } from "react";
import "ol/ol.css";
import Map from "ol/Map";
import TileLayer from "ol/layer/Tile";
import View from "ol/View";
import * as olProj from "ol/proj";
import VectorSource from "ol/source/Vector";
import { Icon, Style } from "ol/style";
import { Vector as VectorLayer } from "ol/layer";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import Draw from "ol/interaction/Draw";
import Snap from "ol/interaction/Snap";
import TileWMS from "ol/source/TileWMS";
import { Modal } from "bootstrap";
import { Link } from "react-router-dom";
import { Circle as CircleStyle } from "ol/style";
import OSM from "ol/source/OSM";

class RegistrarParada extends React.Component {
  constructor(props) {
    super(props);
    this.crearParada = this.crearParada.bind(this);
    this.state = {
      parada: "",
      ubicacionParada: "",
    };

    this.ubicacionParada = "";
  }

  // uso componentDidMount para que corra este script luego de cargar el div de mapa que esta debajo de esto
  componentDidMount() {
    // para mostrar el modal apenas carga la pagina
    var modal = new Modal(document.getElementById("registrarParada"), {});
    modal.show();
    // me traigo las coordenadas del localsotrage
    let coordinates = localStorage["coordinates"];
    // lo separo en latitude y longitude
    var latlong = coordinates.split(",");
    const latitude = parseFloat(latlong[0]);
    const longitude = parseFloat(latlong[1]);
    // lo formateo a un formato que acepta openlayers
    var pos = olProj.fromLonLat([longitude, latitude]);

    var source = new VectorSource();

    var mapa = new TileLayer({
      source: new OSM(),
    });
    var parada = new VectorLayer({
      source: source,
      style: new Style({
        fill: new Fill({
          color: "rgba(255, 255, 255, 0.2)",
        }),
        stroke: new Stroke({
          color: "#ffcc33",
          width: 5,
        }),
        image: new CircleStyle({
          radius: 7,
          fill: new Fill({
            color: "#ffcc33",
          }),
        }),
      }),
    });

    var layers = [mapa, parada];

    var map = new Map({
      layers: layers,
      target: "map2",
      view: new View({
        center: pos,
        zoom: 16,
      }),
    });

    var draw, snap; // global so we can remove them later
    var typeSelect = "Point";

    function addInteractions() {
      draw = new Draw({
        source: source,
        type: typeSelect,
      });
      map.addInteraction(draw);
      snap = new Snap({ source: source });
      map.addInteraction(snap);

      draw.on("drawend", function (e2) {
        var feature = e2.feature;
        var geometry = feature.getGeometry();

        //depending on the type of geometry drawn you may get first and last
        //coordinate. From your description I guess you draw a linestring
        //you may clarify that using geometry.getType()
        //so for ol.geom.LineString do as follows. According to the
        //documentation this should work for any type of geometries
        var startCoord = geometry.getFirstCoordinate();
        var endCoord = geometry.getLastCoordinate();
        //If you are not sure what the type is, or if you face any problems
        //with getFirstCoordinate, getLastCoordinate
        //you may go for a more general technique

        this.ubicacionParada = geometry.getCoordinates();
        //and then parse the coordinates object to get first and last
        var startCoord = this.ubicacionParada[0];
        var endCoord = this.ubicacionParada[this.ubicacionParada.length - 1];

        localStorage["ubicacionParada"] = "";
        localStorage["ubicacionParada"] = JSON.stringify(this.ubicacionParada);
      });
    }

    addInteractions();
  }

  showToast() {
    var toast = document.getElementById("toastMessage");
    toast.className += " show"; // muestro el toast
  }

  actualizarubicacionParadaEnState(r) {
    this.setState({ ubicacionParada: this.r });
  }
  crearParada = async () => {
    console.log("crearParada");
    this.ubicacionParada = localStorage["ubicacionParada"];

    var ubicacionParada = {
      type: "point",
      coordinates: JSON.parse(localStorage["ubicacionParada"]),
    };

    this.setState({ ubicacionParada: ubicacionParada });

    const body = {
      nombre: this.state.parada,
      punto: ubicacionParada,
    };

    const settings = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(body),
    };

    const request = await fetch(
      "http://localhost:4567/paradas/add",
      //"http://localhost:4567/paradas/crearparada",
      settings
    );
    console.log("1");
    const response = await request.json();
    console.log("response:");
    console.log(response);
    window.location.reload(false);
  };

  render() {
    return (
      <div>
        <div id="map2" className="map"></div>
        <div
          className="modal fade"
          id="registrarParada"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Registrar una Parada</h5>
              </div>
              <div className="modal-body">
                <form>
                  <div className="mb-3">
                    <label for="parada" className="form-label">
                      Nombre de la parada
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="parada"
                      value={this.state.parada}
                      onChange={(event) => {
                        this.setState({ parada: event.target.value });
                      }}
                    ></input>
                  </div>
                  <div className="d-grid gap-2">
                    <button
                      type="button"
                      className="btn btn-success"
                      onClick={this.showToast}
                      data-bs-dismiss="modal"
                    >
                      Marcar parada en el mapa
                    </button>
                    <Link to={"/home"} className="link btn btn-danger btn-sm">
                      Cancelar
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div className="position-fixed bottom-0 end-0 p-3">
          <div
            id="toastMessage"
            className="toast"
            role="alert"
            aria-live="assertive"
            aria-atomic="true"
          >
            <div className="toast-body">
              Marque en el mapa con un click, la ubicacion de la parada
              <div className="mt-2 pt-2 border-top d-grid gap-2">
                <button
                  data-bs-toggle="modal"
                  data-bs-target="#successModal"
                  type="button"
                  className="btn btn-success btn-sm"
                  data-bs-dismiss="toast"
                  onClick={this.crearParada}
                >
                  Crear Parada
                </button>

                <Link to={"/home"} className="link btn btn-danger btn-sm">
                  Salir
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div
          className="modal fade"
          id="successModal"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Parada Creada</h5>
              </div>
              <div className="modal-body d-grid gap-2 text-center">
                <p>Se cre&oacute; la parada correctamente</p>
                <Link to="/home" className="link btn btn-success">
                  Volver al inicio
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RegistrarParada;
