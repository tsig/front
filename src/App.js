import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./components/NavBar/NavBar";
import Home from "./components/Home/Home";
import RegistrarLinea from "./components/RegistrarLinea/RegistrarLinea";
import RegistrarParada from "./components/RegistrarParada/RegistrarParada";
import PageError from "./components/PageError/PageError";
import Login from "./components/Login/Login";
import "./App.css";
import Loading from "./components/Loading/Loading";
import EditarParada from "./components/EditarParada/EditarParada";
import EditarLinea from "./components/editarLinea/EditarLinea";
import BuscarCruce from "./components/BuscarCruce/BuscarCruce";
import BusquedaPorModHora from "./components/BusquedaPorModHora/BusquedaPorModHora";
import BuscarPorEmpresa from "./components/BuscarPorEmpresa/BuscarPorEmpresa";
import BuscarLineaDestino from "./components/BuscarLineaDestino/BuscarLineaDestino";
import { useState, useEffect } from "react";

const App = () => {
  const [loading, setLoading] = useState(true);
  const [showModalSearchAddress, setShowModalSearchAddress] = useState(false);
  const [showModalSearchLine, setShowModalSearchLine] = useState(false);
  const [filterParadas, setFilterParadas] = useState("true");
  const [drawLinesPolygon, setDrawLinesPolygon] = useState(false)

  useEffect(() => {
    asyncCall().then(() => setLoading(false));
  }, []);

  if (loading) {
    return <Loading />;
  }

  function asyncCall() {
    return new Promise((resolve) => setTimeout(() => resolve(), 2500)); // poner aca cuando cargue, por defecto hay un tiempo
  }

  const toggleShowModalSearchAddress = () =>
    setShowModalSearchAddress(!showModalSearchAddress);

  const toggleShowModalSearchLine = () =>
    setShowModalSearchLine(!showModalSearchLine);

  const toggleFilterParadas = (value) => setFilterParadas(value);

  const toggleDrawLinesPolygon = () => setDrawLinesPolygon(!drawLinesPolygon)

  return (
    <BrowserRouter>
      <div>
        <NavBar
          toggleShowModalSearchAddress={toggleShowModalSearchAddress}
          toggleShowModalSearchLine={toggleShowModalSearchLine}
          toggleFilterParadas={toggleFilterParadas}
          toggleDrawLinesPolygon={toggleDrawLinesPolygon}
        />
        <Login />
        <Redirect from="/" to="/home" />
        <Switch>
          <Route
            path="/home"
            component={() => (
              <Home
                showModalSearchAddress={showModalSearchAddress}
                showModalSearchLine={showModalSearchLine}
                toggleShowModalSearchAddress={toggleShowModalSearchAddress}
                toggleShowModalSearchLine={toggleShowModalSearchLine}
                filterParadas={filterParadas}
                drawLinesPolygon={drawLinesPolygon}
              />
            )}
          />
          <Route path="/RegistrarLinea" component={RegistrarLinea} />
          <Route path="/RegistrarParada" component={RegistrarParada} />
          <Route path="/EditarParada" component={EditarParada} />
          <Route path="/EditarLinea" component={EditarLinea} />
          <Route path="/BuscarCruce" component={BuscarCruce} />
          <Route path="/BusquedaPorModHora" component={BusquedaPorModHora} />
          <Route path="/BuscarPorEmpresa" component={BuscarPorEmpresa} />
          <Route path="/BuscarLineaDestino" component={BuscarLineaDestino} />
          <Route component={PageError} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default App;
